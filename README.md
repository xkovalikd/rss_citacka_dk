stranka je dostupna na adrese: https://xkovalikd.gitlab.io/rss_citacka_dk/

Povodne zadanie som prerobil, aby som to nerendroval server side a navyse, zvoleny framework svelte mi stazoval pracu na tomto zadani (kedze obshoval css,html a js v jednom subore),
tak som sa rozhodol ho prerobit, kedze uprava povodneho mi zabrala vela casu a nikam to neviedlo. 

Pouzil som GitLab Pages.
Zdrojovy kod, rovnako aj CI/CD pipeliny su dostupne na: https://gitlab.com/xkovalikd/rss_citacka_dk

Obsah suboru .yml potrebny pre CI/CD je nasledovny: (v jednom jobe sa vykona aj produkcny bulid, aj deploy)
image: node:8.12.0

pages:
  cache:
    paths:
    - node_modules/

  stage: deploy
  script:
  - npm install -g @angular/cli@6.2.1					// nainstalovanie potrebnych dependencies
  - npm install								// nainstalovanie potrebnych dependencies
  - ng build --prod --output-path docs --base-href rss_citacka_dk	// prikaz na vybuildovanie aplikacie na produkcne pouzitie do priecinka docs
  - mv docs/* public/							// obsah priecinka docs bolo potrebne premiestnit do priecinka public, kedze gitlab pages to vyzaduje
  artifacts:								// deploy artefaktov, ktore su v priecinku public (po vybuildovani appky)								
    paths:
    - public
  only:
  - master
  - pages

Po kazdej zmene v repozitari (commite) sa automaticky spustia pipeliny a job, ktory automaticky vybuilduje a deployne aktualnu verziu rss citacky.



# rss_citacka_dk

This project was generated with [Angular CLI]

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Production
For production run `ng build --prod`.
